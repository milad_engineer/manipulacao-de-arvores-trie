/*
 * UNIVERSIDADE CATÓLICA DOM BOSCO
 * TRABALHO DE ESTRUTURA DE DADOS II
 * PROFESSOR: MARCOS ALVES
 */
package trabalhotries;
import java.util.ArrayList;
/**
 * @data de início 27/04/2017 
 * @data de término 10/0/2017 
 * @author MILAD ROGHANIAN RA160985
 */
public class NoTrie 
{
    //################ VARIÁVEIS #############################
    private String symbol; //Símbolo
    private NoTrie son;//Filho abaixo
    private NoTrie Brother;//Irmão 
    private int indexin;//Posição inicial 
    private int indexout;//Posição final
    
    /**
     * @return the symbol
     */
    public String getSymbol() {
        return symbol;
    }

    /**
     * @param symbol the symbol to set
     */
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    /**
     * @return the son
     */
    public NoTrie getSon() {
        return son;
    }

    /**
     * @param son the son to set
     */
    public void setSon(NoTrie son) {
        this.son = son;
    }

    /**
     * @return the Brother
     */
    public NoTrie getBrother() {
        return Brother;
    }

    /**
     * @param Brother the Brother to set
     */
    public void setBrother(NoTrie Brother) {
        this.Brother = Brother;
    }

    /**
     * @return the indexin
     */
    public int getIndexin() {
        return indexin;
    }

    /**
     * @param indexin the indexin to set
     */
    public void setIndexin(int indexin) {
        this.indexin = indexin;
    }

    /**
     * @return the indexout
     */
    public int getIndexout() {
        return indexout;
    }

    /**
     * @param indexout the indexout to set
     */
    public void setIndexout(int indexout) {
        this.indexout = indexout;
    }
}















































































//Autor MILAD ROGHANIAN RA160985