/*
 * UNIVERSIDADE CATÓLICA DOM BOSCO
 * TRABALHO DE ESTRUTURA DE DADOS II
 * PROFESSOR: MARCOS ALVES
 */
package trabalhotries;
import java.util.ArrayList;
import javax.swing.JOptionPane;
/** COMPLETO
 * @data de início 27/04/2017 
 * @data de término 10/0/2017 
 * @author MILAD ROGHANIAN RA160985
 */

public class Controle 
{
    //################ VARIÁVEIS #############################
    private NoTrie raiz = new NoTrie(); //É a raiz da árvore trie
    private String wordout = new String();//é a String com o texto a ser mostrado na tela
    private String word = new String();//É a String com o texto digitado pelo usuário
    
    //################ INICIAR #############################
    //função que inicia os primeiros processamentos da String
    public void iniciar () 
    {
        int i=0;
        setWord(JOptionPane.showInputDialog("Digite a frase")); //Janela com campo onde o usuário digita a frase    
        setWord(getWord().replace(" ", "")); //Tira os espaços em branco da String
        setWord(this.word + '$');//Insere um '$' no final da String
    }
    
    //################ INSERE_TRIE #############################
    //função que manipula outras funções para fazer a inserção e concatenação da árvore Trie
    public void insere_trie()
    {
        //For para inserir na Trie
        for (int i=this.word.length()-1; i>=0; i--)//for vai do tamanho da palavra até zero
        {
            //esta linha de código manda inserir na árvore Char por Char da String word do fim ao início
            this.raiz.setSon(insere_string(this.raiz.getSon(),this.word.substring(i,this.word.length()),0,i));
        }
        
        //For para concatenar a Trie em árvore Patricia e finalmente em Sufixos
        for (int i=0; i<this.word.length()-1; i++) //
        {
            //esta linha de código manda concatenar a árvore o tanto de vezes referente ao tamanho da String word
            this.raiz.setSon(patricia(this.raiz.getSon()));
        }
        
    }
    
    //################ INSERE_STRING #############################
    //função que insere uma sequencia de char em uma String na árvore Trie 
    //Também insere as posições inicil e final na Trie de cada char da string word
    public NoTrie insere_string(NoTrie no, String sword, int j, int indice) //i começa com 0 e a palavra já virá com $
    {
        if (j!=sword.length()) {
            if(no==null){
                no = new NoTrie();
                no.setSymbol(Character.toString(sword.charAt(j)));
                no.setIndexin(indice);
                no.setIndexout(indice);
                no.setSon(insere_string(no.getSon(),sword,j+1,indice+1));
            } else {
                if (no.getSymbol().charAt(0)==sword.charAt(j)) {
                    no.setSon(insere_string(no.getSon(),sword,j+1,indice+1));
                } else {
                    if (no.getBrother()==null){
                        no.setBrother(insere_string(no.getBrother(),sword,j,indice));
                    } else { 
                        no.setBrother(insere_string(no.getBrother(),sword,j,indice));
                    }
                }
            }
        }    
        return no;   
    }
    
    //################ PATRICIA #############################
    //função que concatena 2 chars da árvore Trie 
    public NoTrie patricia(NoTrie no)
    {        
        NoTrie aux = no;
        while (aux != null) {
           if (aux.getSon() != null) {
               if (aux.getSon().getBrother() != null) {
                   aux.setSon(patricia(aux.getSon()));
               }else {
                    aux.setSymbol(aux.getSymbol() + aux.getSon().getSymbol());
                    aux.setIndexout(aux.getSon().getIndexout());
                    aux.setSon(aux.getSon().getSon());
               }
           }
           aux = aux.getBrother();
        }
        return no;
    }
    
    //################ IMPRIME #############################
    //função que imprime cada palavra da árvore Trie junto com seus índices
    public NoTrie imprime (NoTrie no)
    {
        if (no!=null){
            if (no.getBrother()!=null){
                //this.setWordout(this.wordout + no.getSymbol() + "(" +no.getIndexin() + ","+ no.getIndexout() + ")" + " -- ");
                imprime(no.getBrother());
                imprime(no.getSon());
            }       
            this.setWordout(this.wordout + no.getSymbol() + "(" +no.getIndexin() + ","+ no.getIndexout() + ")" + "\n");
        }
        return no;
    }

    /**
     * @return the wordout
     */
    public String getWordout(){
        return wordout;
    }

    /**
     * @param wordout the wordout to set
     */
    public void setWordout(String wordout) {
        this.wordout = wordout;
    }

    /**
     * @return the word
     */
    public String getWord() {
        return word;
    }

    /**
     * @param word the word to set
     */
    public void setWord(String word) {
        this.word = word;
    }

    /**
     * @return the raiz
     */
    public NoTrie getRaiz() {
        return raiz;
    }

    /**
     * @param raiz the raiz to set
     */
    public void setRaiz(NoTrie raiz) {
        this.raiz = raiz;
    }
}















































































//Autor MILAD ROGHANIAN RA160985